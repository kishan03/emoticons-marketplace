import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Product from './Product'
import AppBar from '@material-ui/core/AppBar'
import Tab from '@material-ui/core/Tab'


const styles = theme => ({
    root: {
      width: '100%',
      marginTop: theme.spacing.unit * 3,
      overflowX: 'auto',
    },
    table: {
      minWidth: 700,
    },
})


class Dashboard extends Component {
    state = {
        message: "not at bottom"
    }

    handleChange = (e) => {
        this.props.handleSortProducts(e)
    }

    render(){
        const { products } = this.props
        
        return(
            <div>
                <AppBar position="static" title="Products List">
                    <Grid container spacing={24}>
                        <Grid item xs={3}>
                            <Tab label="Faces" />
                        </Grid>
                        <Grid item xs={2}>
                            <Tab label="IDs" onClick={() => this.handleChange("id")} />
                        </Grid>
                        <Grid item xs={2}>
                            <Tab label="Price" onClick={() => this.handleChange("price")} />
                        </Grid>
                        <Grid item xs={2}>
                            <Tab label="Size" onClick={() => this.handleChange("size")} />
                        </Grid>
                        <Grid item xs={3}>
                            <Tab label="Date Added" onClick={() => this.handleChange("date")} />
                        </Grid>
                    </Grid>
                </AppBar>
                <Grid container spacing={24}>
                    <Grid item xs={12}>
                        {products.map(function(product, index) {
                            if ((index%20) === 0 && index > 0){
                                return  <img
                                            className="ad" 
                                            alt="sponsor" 
                                            src={`http://localhost:3000/ads/?r=${Math.floor(Math.random()*1000)}`} />
                            }
                            else {
                                return <Product key={product.id} product={product} />
                            }
                        })}
                    </Grid>
                </Grid>
            </div>
        )
    }
}

Dashboard.propTypes = {
    classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(connect()(Dashboard))