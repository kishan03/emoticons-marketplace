import React, { Component } from 'react'
import Dashboard from './Dashboard'
import { connect } from 'react-redux'
import * as ProductsAPI from '../utils/_DATA'


class App extends Component {
    state = {
        products : [],
        page: 1,
        scrolling: false,
        endOfPage: false,
    }
    
    componentWillMount(){
        this.loadProducts()
        this.scrollListener = window.addEventListener('scroll', (e) => {
            this.handleScroll(e)
        })
    }

    handleScroll = (e) => {
        const { scrolling, endOfPage } = this.state
        if (scrolling || endOfPage) return
        const windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
        const body = document.body;
        const html = document.documentElement;
        const docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight,  html.scrollHeight, html.offsetHeight);
        const windowBottom = windowHeight + window.pageYOffset;    
        if (windowBottom >= (docHeight - 100)) {
            this.loadMore()
        }
    }

    loadProducts = () => {
        const { page } = this.state
        ProductsAPI._getProducts(page)
        .then((products) => {
            if (products.length === 0) {
                this.setState({
                    endOfPage: true,
                })
            }
            else {
                this.setState((currentState) => ({
                    products: currentState.products.concat(products),
                    scrolling: false,
                }))
            }
        })
    }

    loadMore = () => {
        this.setState(prevState => ({
            page: prevState.page + 1,
            scrolling: true,
        }), this.loadProducts)
    }

    handleSortProducts = (sortBy) => {
        const { page } = this.state
        ProductsAPI._getProducts(page, sortBy)
        .then((products) => {
            this.setState((currentState) => ({
                products,
            }))
        })
    }

    render() {
        const {products, endOfPage} = this.state    
        return (
            <div className="container">
                <header>
                    <center>
                        <h1>Products Grid</h1>

                        <p>Here you're sure to find a bargain on some of the finest ascii available to purchase. Be sure to peruse our selection of ascii faces in an exciting range of sizes and prices.</p>

                        <p>But first, a word from our sponsors:</p> 
                        <img className="ad" alt="sponsor" src={`http://localhost:3000/ads/?r=${Math.floor(Math.random()*1000)}`} />
                    </center>
                </header>
                
                <Dashboard products={products} handleSortProducts={this.handleSortProducts} />
                <center>{endOfPage === true ? <p>~ end of catalogue ~</p> : ""}</center>
            </div>

        )
      }
}

export default connect()(App)