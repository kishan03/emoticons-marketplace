import React, { Component } from 'react'
import { formatDate } from '../utils/helpers'
import PropTypes from 'prop-types'
import Grid from '@material-ui/core/Grid'
import { withStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'


const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  },
})


class Product extends Component {
    render() {
        const { product, classes } = this.props

        return (
            <Paper className={classes.root} elevation={5}>
                <Grid container spacing={24}>
                    <Grid item xs={3}>
                        <p style={{fontSize: product.size}}>{product.face}</p>
                    </Grid>
                    <Grid item xs={2}>
                        <p>{product.id}</p>
                    </Grid>
                    <Grid item xs={2}>
                        <p>{"$" + (product.price / 100)}</p>
                    </Grid>
                    <Grid item xs={2}>
                        <p>{product.size}</p>
                    </Grid>
                    <Grid item xs={3}>
                        <p>{formatDate(product.date)}</p>
                    </Grid>
                </Grid>
            </Paper>
        )
    }
}

Product.propTypes = {
  classes: PropTypes.object.isRequired,
  product: PropTypes.array.isRequired,
};

export default withStyles(styles)(Product)