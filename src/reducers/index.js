import { combineReducers } from 'redux'
import products from './products'
import { loadingBarReducer } from 'react-redux-loading'

export default combineReducers({
    products,
    loadingBar: loadingBarReducer,
})