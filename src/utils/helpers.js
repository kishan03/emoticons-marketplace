function singularPluralDecider(days) {
    return days === 1 ? "day" : "days"
}

export function formatDate (timestamp) {
    const d = new Date(timestamp)
    var diff = (new Date() - d)/1000
    diff = Math.abs(Math.floor(diff)) 
    var days = Math.floor(diff/(24*60*60));

    if (days <= 7) {
        return `${days} ${singularPluralDecider(days)} ago`
    }
    else{
        const time = d.toLocaleTimeString('en-US')
        return time.substr(0, 5) + time.slice(-2) + ' | ' + d.toLocaleDateString()
    }
}