export const _getProducts = (pageindex=1, sort_by="", limit=10) => 
	fetch(`http://localhost:3000/products?_limit=${limit}&_page=${pageindex}&_sort=${sort_by}`)
	.then((data) => data.json())
	.then(data => data)