import {
  _getProducts,
} from './_DATA.js'

export function getInitialData () {
  return Promise.all([
    _getProducts(),
  ]).then(([products]) => ({
    products
  }))
}
