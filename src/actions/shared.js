import { getInitialData } from '../utils/api'
import { receiveProducts } from './products'


export function handleInitialData() {
	return (dispatch) => getInitialData()
	.then((products) => {
		dispatch(receiveProducts(products)
	)})
}